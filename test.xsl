<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="body">
  
  <html>
  <body>
    <table border="1">
      
      <tr>
	      <td>id</td>
	      <td>routeTag</td>
	      <td>dirTag</td>
	      <td>lat</td>
	      <td>lon</td>
	      <td>secsSinceReport</td>
	      <td>predictable</td>
	      <td>heading</td>
      </tr>
      
      <xsl:for-each select="vehicle">
        <tr>
		<xsl:for-each select="@*">
		<td><xsl:value-of select="."/></td>
		</xsl:for-each>
        </tr>
      </xsl:for-each>
      
    </table>
  </body>
  </html>
  
</xsl:template>

</xsl:stylesheet>
