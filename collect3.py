#!/usr/bin/python

# gather xml data for some purpose
# dsm.iv.tr@gmail.com
# 2015
# gpl, open source, yada-yada

import sys, SimpleHTTPServer, SocketServer, urlparse, urllib2, cgi, datetime

# standard html stuff
def html_start():
    return """
    <html>
        <head>
            <title>Vehicles</title>
        </head>

        <body>
    """

def html_end():
    return """
        </body>
        </html>
    """

def content_home():
    return """
        <h1>Jesse Enjoys The Transit (1.0 beta)</h1>
        <h3>Arbitrary Vehicles</h3>
        <p>Enter up to six ID's to request, then submit the form.</p>
        <h3>Range of Vehicles</h3>
        <p>Enter the starting and ending vehicle number, then submit the form.</p>
        <h3>Notes</h3>
        <p>If you fail to enter <em>both</em> the range start and end, the program reverts to the default 'arbitrary vehicle' mode and provides data for any numbers entered.</p>
        <hr/>
        <form name='testform' action='/collect_xmls' method='POST'>
        <div>
            <label for='sched1'>ID</label>
            <input type='text' name='sched1'/>
        </div>

        <br/>

        <div>
            <label for='sched2'>ID</label>
            <input type='text' name='sched2'/>
        </div>

        <br/>

        <div>
            <label for='sched3'>ID</label>
            <input type='text' name='sched3'/>
        </div>

        <br/>

        <div>
            <label for='sched4'>ID</label>
            <input type='text' name='sched4'/>
        </div>

        <br/>

        <div>
            <label for='sched5'>ID</label>
            <input type='text' name='sched5'/>
        </div>

        <br/>

        <div>
            <label for='sched6'>ID</label>
            <input type='text' name='sched6'/>
        </div>

        <br/>

        <div>
            <label for='rangest'>Range Start</label>
            <input type='text' name='rangest'/><br/>
            <label for='rangend'>Range End</label>
            <input type='text' name='rangend'/>
        </div>

        <div>
            <button type='submit'>Get XML</button>
        </div>

        </form>
    """

class AHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        if (self.path == '/'):
            self.send_response(200)
            self.send_header('Content-Type', 'text/html; charset=utf-8')
            self.end_headers()

            self.wfile.write(html_start())
            self.wfile.write(content_home())
            self.wfile.write(html_end())
        else:
            # passthrough handler
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-Type', 'text/html; charset=utf-8')
        self.end_headers()

        self.wfile.write(html_start())

        if (self.path == '/collect_xmls'):
            # open a file
            flname = 'combinedXML_' + str(datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')) + '.xml'
            try:
                flhandle = open(flname, 'w')
                self.wfile.write(log_out('Opened ' + flname + '.<br/>'))
            except IOError:
                self.wfile.write(log_out('Unable to open ' + flname + ' for writing. This is a pretty serious problem, cannot continue.<br/>'))
                sys.exit(2)

            # get a dict of POST data
            post_len = int(self.headers.getheader('content-length', 0))
            post_fields = urlparse.parse_qs(self.rfile.read(post_len))

            if ('rangest' in post_fields.keys()) and ('rangend' in post_fields.keys()):
                self.wfile.write(log_out('Working on range submission.<br/>'))
                for reqn in range(int(post_fields["rangest"][0]), (int(post_fields["rangend"][0]) + 1), 1):
                    reqstr = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocation&a=ttc&t=1000&v=' + cgi.escape(str(reqn))
                    self.wfile.write(log_out('Retrieving ' + reqstr + '.<br/>'))

                    try:
                        resp = urllib2.urlopen(urllib2.Request(reqstr))
                        flhandle.write(resp.read())
                    except URLError:
                        self.wfile.write(log_out('There was a problem getting hold of ' + reqstr + ', moving on.<br/>'))
            else:
                for fieldval in post_fields.values():
                    self.wfile.write(log_out('Assuming arbitrary values given due to lack of complete range.<br/>'))
                    reqstr = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocation&a=ttc&t=1000&v=' + cgi.escape(str(fieldval[0]))
                    self.wfile.write(log_out('Retrieving ' + reqstr + '.<br/>'))

                    try:
                        resp = urllib2.urlopen(urllib2.Request(reqstr))
                        flhandle.write(resp.read())
                    except URLError:
                        self.wfile.write(log_out('There was a problem getting hold of ' + reqstr + ', moving on.<br/>'))

            flhandle.close()

            try:
                flhandle2 = open(flname, 'r')
            except IOError:
                self.wfile.write(log_out('Unable to open ' + flname + ' for writing. This is a pretty serious problem, cannot continue.<br/>'))
                sys.exit(2)

            totalxml = flhandle2.readlines()
            flhandle2.close()

            try:
                flhandle3 = open(flname, 'w')
            except IOError:
                self.wfile.write(log_out('Unable to open ' + flname + ' for writing. This is a pretty serious problem, cannot continue.<br/>'))
                sys.exit(2)

            self.wfile.write(log_out('Combining XML data in ' + flname + '.<br/>'))

            flhandle3.write('<?xml version="1.0" encoding="utf-8" ?>')
            flhandle3.write('<body>')

            for line in totalxml:
                if 'xml' not in line:
                    if 'body' not in line:
                        flhandle3.write(line)

            flhandle3.write('</body>')

            flhandle3.close()

            self.wfile.write(log_out("Generated <a href='" + flname + "'>" + flname + "</a> from XML input. Right-click to save.<br/><br/> <a href='/'>&lt;- do it again!</a>"))
            self.wfile.write(html_end())

def log_out(strout):
    print strout
    return strout

def main(argv):
    port = 8008

    SocketServer.ThreadingTCPServer.allow_reuse_address = True

    httpd = SocketServer.ThreadingTCPServer(("", int(port)), AHandler)
    log_out('App is running. Use CTRL-C in this console to quit. Point your web browser to http://localhost:' + str(port) + '.')

    try:
        httpd.serve_forever()
    except:
        log_out('\nCaught signal, stopping the httpd.')
        httpd.server_close()

if __name__ == '__main__':
    main(sys.argv)
